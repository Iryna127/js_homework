//Сообщение
alert("Hello, Igor! Have a nice day!");

//Присвоение значений
let users = 12345;
let schoolName = "IT School 'Hillel'";
let noUsers = false;

//Определяем тип созданных переменных
console.log(typeof 12345);
console.log(typeof "IT School 'Hillel'");
console.log(typeof false);

//Отображение переменной и ее типа
console.log('value:', users, 'type:', typeof users);
console.log('value:', schoolName, 'type:', typeof schoolName);
console.log('value:', noUsers, 'type:', typeof noUsers);

